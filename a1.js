let http = require("http");

// GET METHOD
http.createServer(function (request, response) {
	
	if(request.url == "/" && request.method == "GET"){
		response.end('Welcome to booking system.');

	} else if(request.url == "/profile" && request.method == "GET"){
		response.end('Welcome to your profile.');
	} else if(request.url == "/courses" && request.method == "GET"){
		response.end("Here's our courses available.");
	}

// POST METHOD
	if(request.url == "/addCourse" && request.method == "POST"){
		response.end('Add course to our resource');

	}

// PUT METHOD
	if(request.url == "/updateCourse" && request.method == "PUT"){
		response.end('Update a course to our resource');

	}
// DELETE METHOD
	if(request.url == "/archiveCourse" && request.method == "DELETE"){
		response.end('Archive course to our resource');

	}
}).listen(4000);

console.log('Server running at localhost:4000');


